package model;

import model.enumeration.CoinFace;
import model.interfaces.Coin;
import java.util.Random;

public class CoinImpl implements Coin {
	
	private int number;
	private CoinFace face;
	private Random rand = new Random();
	
	public CoinImpl(int number) {
		this.number = number;
		if(rand.nextBoolean()){
			this.face = CoinFace.HEADS;
		}
		else {
			this.face = CoinFace.TAILS;
		}
	}
	
	@Override
	public int getNumber() {
		return this.number;
	}

	@Override
	public CoinFace getFace() {
		return this.face;
	}

	@Override
	public void flip() {
		if(this.face==CoinFace.HEADS) {
			this.face = CoinFace.TAILS;
		}
		else {
			this.face = CoinFace.HEADS;
		}

	}

	@Override
	public int hashCode() {
		return this.face.name().hashCode();
	}
	
	@Override
	public boolean equals(Coin coin) {
		return this.face.name().equals(coin.getFace().name());
	}
	
	@Override
	public boolean equals(Object coin) {
		try {
			return this.equals((Coin) coin);
		}
		catch(ClassCastException ex) {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return String.format("Coin %d: %s", this.number, this.face.name());
	}

}
