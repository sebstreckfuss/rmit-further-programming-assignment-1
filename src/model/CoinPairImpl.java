package model;

import model.CoinImpl;
import model.interfaces.Coin;
import model.interfaces.CoinPair;

public class CoinPairImpl implements CoinPair {
	
	private Coin coin1;
	private Coin coin2;
	
	public CoinPairImpl() {
		this.coin1 = new CoinImpl(1);
		this.coin2 = new CoinImpl(2);
	}

	@Override
	public Coin getCoin1() {
		return this.coin1;
	}

	@Override
	public Coin getCoin2() {
		return this.coin2;
	}

	@Override
	public boolean equals(CoinPair coinPair) {
		// TODO Auto-generated method stub
		return  this.coin1.equals(coinPair.getCoin1()) &&
				this.coin2.equals(coinPair.getCoin2());
	}
	
	@Override
	public int hashCode() {
		return this.coin1.hashCode() * this.coin2.hashCode();
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s", 
				this.coin1.toString(), this.coin2.toString());
	}

}
