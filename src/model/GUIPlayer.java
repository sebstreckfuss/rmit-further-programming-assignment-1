package model;

import model.enumeration.BetType;
import model.interfaces.CoinPair;
import model.interfaces.Player;

public class GUIPlayer implements Player{
	private String playerId;
	private String playerName;
	private int points;
	private int bet;
	private BetType betType;
	private CoinPair coinPair;
	
	public GUIPlayer(String playerId, String playerName, int initialPoints) {
		this.playerId = playerId;
		this.playerName = playerName;
		this.points = initialPoints;
		this.resetBet();
	}
	
	@Override
	public String getPlayerName() {
		return this.playerName;
	}

	@Override
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
		
	}

	@Override
	public int getPoints() {
		return this.points;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;		
	}

	@Override
	public String getPlayerId() {
		return this.playerId;
	}

	@Override
	public boolean setBet(int bet) {
		if(bet<=0 || this.points<bet) {
			return false;
		}
		else {
			this.bet = bet;
			return true;
		}
	}

	@Override
	public int getBet() {
		return this.bet;
	}

	@Override
	public void setBetType(BetType betType) {
		this.betType = betType;
		
	}

	@Override
	public BetType getBetType() {
		return this.betType;
	}

	@Override
	public void resetBet() {
		this.bet = 0;
		this.betType = BetType.NO_BET;
		this.setResult(new CoinPairImpl());
		
	}

	@Override
	public CoinPair getResult() {
		return coinPair;
	}

	@Override
	public void setResult(CoinPair coinPair) {
		this.coinPair = coinPair;
		
	}
	
	@Override
	public String toString() {
		// to match the specs
		return String.format(
			"Player: %s, name=%s, bet=%d, betType=%s, points=%d, RESULT .. %s",
				this.playerId, 
				this.playerName, 
				this.bet, 
				this.betType.name(), 
				this.points, 
				this.coinPair==null ? "None" : this.coinPair.toString()
		);
	}
	
	public static void main(String[] args) {
		Player p = new GUIPlayer("1", "gay", 100);
		System.out.println(p.toString());
	}
 
}
