package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import model.enumeration.BetType;
import model.interfaces.Coin;
import model.interfaces.CoinPair;
import model.interfaces.GameEngine;
import model.interfaces.Player;
import view.interfaces.GameEngineCallback;

public class GameEngineImpl implements GameEngine{
	CoinPair coinPair;
	Map<String, Player> players;
	Collection<GameEngineCallback> gameEngineCallbacks;
	
	public GameEngineImpl() {
		players = new HashMap<String, Player>();
		gameEngineCallbacks = new Stack<GameEngineCallback>();
	}
	
	@Override
	public void spinPlayer(Player player, int initialDelay1, int finalDelay1, int delayIncrement1, int initialDelay2,
			int finalDelay2, int delayIncrement2) throws IllegalArgumentException {
		this.checkDelays(initialDelay1, finalDelay1, delayIncrement1);
		this.checkDelays(initialDelay2, finalDelay2, delayIncrement2);
		coinPair = new CoinPairImpl();
		this.spin(coinPair.getCoin1(), player, initialDelay1, finalDelay1, delayIncrement1);
		this.spin(coinPair.getCoin2(), player, initialDelay2, finalDelay2, delayIncrement2);
		player.setResult(coinPair);
		this.playerResults(player, coinPair);
	}

	@Override
	public void spinSpinner(int initialDelay1, int finalDelay1, int delayIncrement1, int initialDelay2, int finalDelay2,
			int delayIncrement2) throws IllegalArgumentException {
		this.checkDelays(initialDelay1, finalDelay1, delayIncrement1);
		this.checkDelays(initialDelay2, finalDelay2, delayIncrement2);
		coinPair = new CoinPairImpl();
		this.spin(coinPair.getCoin1(), initialDelay1, finalDelay1, delayIncrement1);
		this.spin(coinPair.getCoin2(), initialDelay2, finalDelay2, delayIncrement2);
		this.spinnerResults(coinPair);
		this.applyBetResults(coinPair);
	}
	
	private void spin(Coin coin, int n, int end, int increment) {
		while(n<end) {
			coin.flip();
			this.spinnerCoinUpdate(coin);
			try {
				Thread.sleep(n);
			} 
			catch (InterruptedException e) {}
			n+=increment;
		}
	}
	
	private void spin(Coin coin, Player player, int n, int end, int increment) {
		while(n<end) {
			coin.flip();
			this.playerCoinUpdate(player, coin);
			try {
				Thread.sleep(n);
			} 
			catch (InterruptedException e) {}
			n+=increment;
		}
	}
	
	private void checkDelays(int initial, int finalD, int increment) throws IllegalArgumentException{
		if(initial<0 || finalD<0 || increment>(finalD-initial)) {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public void applyBetResults(CoinPair spinnerResult) { 
		for(Player player : this.players.values()) {
			player.getBetType().applyWinLoss(player, spinnerResult);
		}
		
	}

	@Override
	public void addPlayer(Player player) {
		this.players.put(player.getPlayerId(), player);		
	}

	@Override
	public Player getPlayer(String id) {
		for(Player player : this.players.values()) {
			if(player.getPlayerId().equals(id)) {
				return player;
			}
		}
		return null;
	}

	@Override
	public boolean removePlayer(Player player) {
		if(this.players.containsValue(player)) {
			this.players.remove(player.getPlayerId());
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {
		this.gameEngineCallbacks.add(gameEngineCallback);
	}

	@Override
	public boolean removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
		return this.gameEngineCallbacks.remove(gameEngineCallback);
	}

	@Override
	public Collection<Player> getAllPlayers() {
		return this.players.values();
	}

	@Override
	public boolean placeBet(Player player, int bet, BetType betType) {
		if(bet<=0 || player.getPoints()<bet || betType==BetType.NO_BET) {
			player.resetBet();
			return false;
		}
		else {
			player.setBet(bet);
			player.setBetType(betType);
			return true;
		}
	}
	
	private void playerCoinUpdate(Player player, Coin coin) {
		for(GameEngineCallback c : this.gameEngineCallbacks) {
			c.playerCoinUpdate(player, coin, this);
		}
	}
	
	private void spinnerCoinUpdate(Coin coin) {
		for(GameEngineCallback c : this.gameEngineCallbacks) {
			c.spinnerCoinUpdate(coin, this);
		}
	}
	
	private void playerResults(Player player, CoinPair coinPair) {
		for(GameEngineCallback c : this.gameEngineCallbacks) {
			c.playerResult(player, coinPair, this);
		}
	}
	private void spinnerResults(CoinPair coinPair) {
		for(GameEngineCallback c : this.gameEngineCallbacks) {
			c.spinnerResult(coinPair, this);
		}
	}
}
