package model.enumeration;

import model.interfaces.CoinPair;
import model.interfaces.Player;

/**
 * Provided enum type for Further Programming representing a type of Bet<br>
 * See inline comments for where you need to provide additional code
 * 
 * @author Caspar Ryan
 * 
 */
public enum BetType
{
      COIN1 {
         @Override
         public void applyWinLoss(Player player, CoinPair spinnerResult)
         {
        	 CoinFace pCoin = player.getResult().getCoin1().getFace();
        	 CoinFace sCoin = spinnerResult.getCoin1().getFace();
        	 if(pCoin.equals(sCoin)) {
        		 player.setPoints(player.getPoints()+player.getBet());
        	 }
        	 else {
        		 player.setPoints(player.getPoints()-player.getBet());
        	 }
         }
      },
      COIN2 {
    	  @Override
    	  public void applyWinLoss(Player player, CoinPair spinnerResult)
    	  {
    		 CoinFace pCoin = player.getResult().getCoin2().getFace();
         	 CoinFace sCoin = spinnerResult.getCoin2().getFace();
    		 if(pCoin.equals(sCoin)) {
         		 player.setPoints(player.getPoints()+player.getBet());
         	 }
         	 else {
         		 player.setPoints(player.getPoints()-player.getBet());
         	 }
    	  }
      },
      NO_BET {
    	  @Override
    	  public void applyWinLoss(Player player, CoinPair spinnerResult) {
    	  }
      },
      BOTH {
    	  @Override
    	  public void applyWinLoss(Player player, CoinPair spinnerResult) {
    		 CoinFace pCoin1 = player.getResult().getCoin1().getFace();
         	 CoinFace pCoin2 = player.getResult().getCoin2().getFace();
         	 
         	 CoinFace sCoin1 = spinnerResult.getCoin1().getFace();
         	 CoinFace sCoin2 = spinnerResult.getCoin2().getFace();
    		 if(pCoin1.equals(sCoin1) && pCoin2.equals(sCoin2)) {
          		 player.setPoints(player.getPoints()+(player.getBet() * 2));
          	 }
          	 else {
          		 player.setPoints(player.getPoints()-player.getBet());
          	 }
    	  }
      };
      
      // TODO finish this class with other enum constants
   
      /**
       * This method is to be overridden for each bet type<br>
       * see assignment specification for betting odds and how win/loss is applied
       * 
       * @param player - the player to apply the win/loss points balance adjustment
       * @param spinnerResult - the CoinPair result of the spinner to compare to
       */
      public abstract void applyWinLoss(Player player, CoinPair spinnerResult);
}