package view;

import java.util.logging.Level;
import java.util.logging.Logger;

import model.interfaces.Coin;
import model.interfaces.CoinPair;
import model.interfaces.GameEngine;
import model.interfaces.Player;
import view.interfaces.GameEngineCallback;

/**
 * 
 * 
 * Skeleton implementation of GameEngineCallback showing Java logging behaviour
 * 
 * @author Caspar Ryan
 * @see view.interfaces.GameEngineCallback
 * 
 */
public class GameEngineCallbackImpl implements GameEngineCallback
{
   private static final Logger logger = Logger.getLogger(GameEngineCallback.class.getName());

   public GameEngineCallbackImpl()
   {
      // NOTE need to also set the console to FINE in %JRE_HOME%\lib\logging.properties
      logger.setLevel(Level.FINE);
   }

   public void playerCoinUpdate(Player player, Coin coin, GameEngine engine)
   {
      // intermediate results logged at Level.FINE
	   String msg = String.format("%s coin %d flipped to %s", 
			   player.getPlayerName(), coin.getNumber(), coin.getFace());
      logger.log(Level.FINE, msg);

   }

   public void playerResult(Player player, CoinPair coinPair, GameEngine engine)
   {
      String msg = String.format("%s, %s",
    		  player.getPlayerName(), coinPair.toString());
      logger.log(Level.INFO, msg);
   }

	@Override
	public void spinnerCoinUpdate(Coin coin, GameEngine engine) {
		String msg = String.format("Spinner coin %d flipped to %s",
				coin.getNumber(), coin.getFace());
		logger.log(Level.FINE, msg);
		
	}
	
	@Override
	public void spinnerResult(CoinPair coinPair, GameEngine engine) {
		logger.log(Level.INFO, "Spinner: " + coinPair.toString());
		
	}
}
